import os
from typing import *
from bag.design import Module
from .params import dc_tb_params

yaml_file = os.path.join(f'{os.environ["BAG_GENERATOR_ROOT"]}/BagModules/dc_tb_templates',
                         'netlist_info', 'dc_tb.yaml')


# noinspection PyPep8Naming
class schematic(Module):
    """Module for library dc_tb_templates cell dc_tb.

    Fill in high level description here.
    """

    def __init__(self, bag_config, parent=None, prj=None, **kwargs):
        Module.__init__(self, bag_config, yaml_file, parent=parent, prj=prj, **kwargs)
       
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """Returns a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : Optional[Dict[str, str]]
            dictionary from parameter names to descriptions.
        """
        return dict(
            params='dc_tb_params',
        )

    def design(self, params: dc_tb_params):
        """To be overridden by subclasses to design this module.

        This method should fill in values for all parameters in
        self.parameters.  To design instances of this module, you can
        call their design() method or any other ways you coded.

        To modify schematic structure, call:

        rename_pin()
        delete_instance()
        replace_instance_master()
        reconnect_instance_terminal()
        restore_instance()
        array_instance()
        """

        # setup bias sources
        self.design_dc_bias_sources(
            vbias_dict=params.sch_params.to_bag_vbias_dict(),
            ibias_dict=params.sch_params.to_bag_ibias_dict(),
            vinst_name='VSUP',
            iinst_name='IBIAS',
            define_vdd=True
        )

        # setup custom cdf parameters (i.e. pwl source 'fileName')
        for instanceName, cdf_params in params.sch_params.instance_cdf_parameters.items():
            self.instances[instanceName].parameters.update(cdf_params)

        # setup DUT
        self.replace_instance_master(
            inst_name='XDUT',
            lib_name=params.dut.lib,
            cell_name=params.dut.cell,
            static=True
        )

        for conn in params.sch_params.dut_conns:
            self.reconnect_instance_terminal(
                inst_name='XDUT',
                term_name=conn.term_name,
                net_name=conn.net_name
            )
