"""
Testbench dc_tb
===============

"""

from sal.testbench_base import TestbenchBase
from .params import dc_tb_params


class testbench(TestbenchBase):
    def __init__(self):
        super().__init__()
        self.params = None

    @property
    def package(self):
        return "dc_tb"

    @classmethod
    def parameter_class(cls):
        """Return the parameter class"""
        return dc_tb_params

    # Define template draw and schematic parameters below using property decorators:
    @property
    def params(self) -> dc_tb_params:
        return self._params

    @params.setter
    def params(self, val: dc_tb_params):
        self._params = val
