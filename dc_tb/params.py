#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import ParamsBase
from sal.simulation.simulation_output import *
from sal.testbench_params import *


@dataclass
class dc_tb_params(TestbenchParamsBase):
    """
    Parameter class for dc_tb

    Args:
    ----

    dut: DUT
        inherited from TestbenchParamsBase, encapsulates the DUT lib and cell

    sch_params: TestbenchSchematicParams
        inherited from TestbenchParamsBase, encapsulates schematic related parameters

    gain_fb: float

    vimax: float

    vimin: float

    vbias: float

    vdd: float

    voutref: float

    vout_start: float

    vout_stop: float

    vout_num: int

    """

    gain_fb: float
    vimax: float
    vimin: float
    vbias: float
    vdd: float
    voutref: float
    vout_start: float
    vout_stop: float
    vout_num: int

    @classmethod
    def builtin_outputs(cls) -> Dict[str, SimulationOutputBase]:
        """
        Builtin outputs are merged into the effective simulation params
        """
        return {
            'vin': SimulationOutputVoltage(analysis='dc', signal='vin', quantity=VoltageQuantity.V),
            'vout': SimulationOutputVoltage(analysis='dc', signal='vout', quantity=VoltageQuantity.V),
            'itot': SimulationOutputCurrent(analysis='dc', signal='VSS', terminal="/VGND/PLUS"),
        }

    @classmethod
    def defaults(cls) -> dc_tb_params:
        return dc_tb_params(
            dut=DUT.placeholder(),  # NOTE: Framework will inject DUT
            dut_wrapper_params=None,  # NOTE: generator can decide to use a wrapper
            sch_params=TestbenchSchematicParams(
                dut_conns=[
                    DUTTerminal(term_name='vbias', net_name='vbias')
                ],
                v_sources=[
                    DCSignalSource(source_name='vbias',
                                   plus_net_name='vbias',
                                   minus_net_name='VSS',
                                   bias_value='vbias',
                                   cdf_parameters={})
                ],
                i_sources=[],
                instance_cdf_parameters={}
            ),
            simulation_params=TestbenchSimulationParams(
                variables={},
                sweeps={},
                outputs={}
            ),
            gain_fb=200.0,
            vimax=1.0,
            vimin=1.0,
            vbias=0.185,
            vdd=1.0,
            voutref=0.5,
            vout_start=0.1,
            vout_stop=0.9,
            vout_num=100,
        )
